import React from 'react';
import PropTypes from 'prop-types';
import { getDate } from '../../helpers/timeHelpers';

import "./styles.css";

type ChatHeaderProps = {
  chatName: string,
  participantsCount: number,
  messagesCount: number,
  lastMessageTime: Date
}

const getParticipantsString = (participantsCount: number) => 
  `${participantsCount} participants`

const getMessagesString = (messagesCount: number) => 
  `${messagesCount} messages`

const getLastMessageTimeString = (lastMessageTime: Date) => 
  `last message at ${getDate(lastMessageTime.toISOString())}`

export const ChatHeader: React.FC<ChatHeaderProps> = ({
  chatName,
  participantsCount,
  messagesCount,
  lastMessageTime
}) => {
  return (
    <div className="chat-header">
      <div className="text">
        {chatName}
      </div>
      <div className="text">
        {getParticipantsString(participantsCount)}
      </div>
      <div className="text">
        {getMessagesString(messagesCount)}
      </div>
      <div className="text">
        {getLastMessageTimeString(lastMessageTime)}
      </div>
    </div>
  );
}

ChatHeader.propTypes = {
  chatName: PropTypes.string.isRequired,
  participantsCount: PropTypes.number.isRequired,
  messagesCount: PropTypes.number.isRequired,
  lastMessageTime: PropTypes.instanceOf(Date).isRequired
}