import React from 'react'
import PropTypes from 'prop-types';
import { IMessage, IUser } from '../../interfaces';
import DateElement from '../DateElement';
import Message from '../Message';
import MyMessage from '../Message/myMessage';

import "./styles.css";

type ChatMainProps = {
  messages: IMessage[],
  currentUser: IUser,
  messageFunction: MessageFunctions
}

type MessageFunctions = {
  onLike: (messageId: string) => void,
  onDelete: (messageId: string) => void
  onEdit: (messageId: string, newText: string) => void
} 

const getMessageElement = (currentUser: IUser) => (message: IMessage, messageFunctions: MessageFunctions) => {
  const { onLike, onDelete, onEdit } = messageFunctions;
  return message.userId === currentUser.id
    ?
    (<MyMessage
      message={message}
      onDelete={() => onDelete(message.id)}
      onEdit={(newText: string) => onEdit(message.id, newText)}
      key={message.id}
    />)
    :
    (<Message
      message={message}
      onLike={() => onLike(message.id)}
      key={message.id}
    />)
}

const getDateElement = (date: Date) => {
  const currentDate = new Date();
  const currentDay = currentDate.getUTCDay();
  if (currentDay - date.getUTCDay() === 1)
    return (
      <DateElement
        dateString="Yesterday"
        key={date.getTime()}
      />
    )
  return (
    <DateElement
      dateString={date.toDateString()}
      key={date.getTime()}
    />
  )
}

const getMessagesElements = (obj: ChatMainProps) => {
  const { messages, messageFunction, currentUser } = obj;
  const elements: JSX.Element[] = [];
  const getMessage = getMessageElement(currentUser);
  const firstMessage = getMessage(messages[0], messageFunction);
  const firstDate = getDateElement(new Date(messages[0].createdAt));
  elements.push(firstDate, firstMessage);

  for (let i = 1; i < messages.length; i++) {
    const currentMessage = messages[i];
    const previousMessage = messages[i - 1];
    const currentDate = new Date(currentMessage.createdAt);
    const currentDay = currentDate.getUTCDay();
    const previousDate = new Date(previousMessage.createdAt);
    const previousDay = previousDate.getUTCDay();
    if (previousDay !== currentDay) {
      const dateElement = getDateElement(currentDate);
      elements.push(dateElement);
    }
    elements.push(getMessage(currentMessage, messageFunction));
  }

  return elements;
}

const ChatMain: React.FC<ChatMainProps> = ({
  messages,
  messageFunction,
  currentUser
}) => {
  return (
    <div className="chat-main">
      {getMessagesElements({ messages, messageFunction, currentUser })}
    </div>
  )
}

ChatMain.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isLiked: PropTypes.bool.isRequired
  }).isRequired).isRequired,
  messageFunction: PropTypes.shape({
    onLike: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired
  }).isRequired,
  currentUser: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired
  }).isRequired
}

export default ChatMain;