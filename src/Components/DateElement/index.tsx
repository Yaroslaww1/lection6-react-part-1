import React from 'react';
import PropTypes from 'prop-types';

import "./styles.css";

type DateElementProps = {
  dateString: string,
}

const DateElement: React.FC<DateElementProps> = ({
  dateString,
}) => {
  return (
    <div className="chat-timestamp">
      <span>
        {dateString}
      </span>
    </div>
  );
}

DateElement.propTypes = {
  dateString: PropTypes.string.isRequired
}

export default DateElement;