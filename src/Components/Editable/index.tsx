const EditableHOC = (whenIsEditing: JSX.Element, whenNoEditing: JSX.Element) => (isEditing: boolean) => {
  if (isEditing)
    return whenIsEditing;
  else
    return whenNoEditing;
}

export default EditableHOC;
