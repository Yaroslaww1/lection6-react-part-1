import React from 'react';
import PropTypes from 'prop-types';
import { getDate } from '../../helpers/timeHelpers';
import { IMessage } from '../../interfaces';

import "./styles.css";

type MessageTimeProps = {
  message: IMessage,
}

const MessageTime: React.FC<MessageTimeProps> = ({
  message
}) => {
  return (
    <div className="message-time">
      {message.editedAt === ""
        ? <div>created at {getDate(message.createdAt)}</div>
        : <div>edited at {getDate(message.editedAt)}</div>
      }
    </div>
  );
}

MessageTime.propTypes = {
  message: PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isLiked: PropTypes.bool.isRequired
  }).isRequired
}

export default MessageTime;
