import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { IMessage } from '../../interfaces';
import EditableHOC from '../Editable';
import DeleteIcon from '../MessageIcons/deleteIcon';
import EditIcon from '../MessageIcons/editIcon';
import MessageTime from './messageTime';

import "./styles.css";
import SaveIcon from '../MessageIcons/saveIcon';

type MyMessageProps = {
  message: IMessage,
  onDelete: () => void,
  onEdit: (newText: string) => void
}

const MyMessage: React.FC<MyMessageProps> = ({
  message,
  onDelete,
  onEdit
}) => {
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [text, setText] = useState<string>(message.text);

  const handleEdit = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newText = event.currentTarget.value as string;
    setText(newText);
  }

  const handleSave = () => {
    onEdit(text);
    setIsEditing(false);
  }

  return (
    <div className="chat-message my-message">
      <div className="message-body">
        {EditableHOC(
          (<input
              type="text" 
              value={text}
              onChange={handleEdit}
            />),
          (<div>{message.text}</div>)
        )(isEditing)}
        <MessageTime
          message={message}
        />
      </div>
      <DeleteIcon
        onDelete={onDelete}
      />
      {EditableHOC(
        (<SaveIcon
          onSave={handleSave}
        />),
        (<EditIcon
          onEdit={() => setIsEditing(true)}
        />)
      )(isEditing)}
    </div>
  );
}

MyMessage.propTypes = {
  message: PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isLiked: PropTypes.bool.isRequired
  }).isRequired,
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired
}

export default MyMessage;
