import React from 'react';
import PropTypes from 'prop-types';
import Icon from './icon';

import "./styles.css";

type DeleteIconProps = {
  onDelete: () => void
}

const DeleteIcon: React.FC<DeleteIconProps> = ({
  onDelete
}) => {
  return (
    <Icon
      iconClassName="fa fa-trash"
      onClick={onDelete}
    />
  );
}

DeleteIcon.propTypes = {
  onDelete: PropTypes.func.isRequired
}

export default DeleteIcon;
