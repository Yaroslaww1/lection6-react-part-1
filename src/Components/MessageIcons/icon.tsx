import React from 'react';
import PropTypes from 'prop-types';

import "./styles.css";

type IconProps = {
  onClick: () => void,
  iconClassName: string
}

const Icon: React.FC<IconProps> = ({
  onClick,
  iconClassName
}) => {
  return (
    <div className="message-icon">
      <i
        className={iconClassName}
        aria-hidden="true"
        onClick={onClick}
      ></i>
    </div>
  );
}

Icon.propTypes = {
  onClick: PropTypes.func.isRequired,
  iconClassName: PropTypes.string.isRequired
}

export default Icon;
