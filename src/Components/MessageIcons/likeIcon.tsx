import React from 'react';
import PropTypes from 'prop-types';
import { IMessage } from '../../interfaces';

import "./styles.css";

type LikeIconProps = {
  message: IMessage,
  onLike: (message: IMessage) => void
}

const LikeIcon: React.FC<LikeIconProps> = ({
  message,
  onLike
}) => {
  return (
    <div className="message-icon like">
      {message.isLiked
        ?
        <i
          className="fa fa-heart"
          aria-hidden="true"
          style={{ color: "red" }}
          onClick={() => onLike(message)}
        ></i>
        :
        <i
          className="fa fa-heart"
          aria-hidden="true"
          onClick={() => onLike(message)}
        ></i>
      }
    </div>
  );
}

LikeIcon.propTypes = {
  onLike: PropTypes.func.isRequired,
  message: PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isLiked: PropTypes.bool.isRequired
  }).isRequired, 
}

export default LikeIcon;
