import React from 'react';
import PropTypes from 'prop-types';
import Icon from './icon';

import "./styles.css";

type SaveIconProps = {
  onSave: () => void
}

const SaveIcon: React.FC<SaveIconProps> = ({
  onSave
}) => {
  return (
    <Icon
      iconClassName="fa fa-floppy-o"
      onClick={onSave}
    />
  );
}

SaveIcon.propTypes = {
  onSave: PropTypes.func.isRequired,
}

export default SaveIcon;
