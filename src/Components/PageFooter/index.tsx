import React from 'react';

import "./styles.css";

const PageFooter: React.FC = () => {
  return (
    <footer>
      © BSA 2020
    </footer>
  );
}

export default PageFooter;
