import React, { useState, useEffect } from 'react';
import Spinner from '../../Components/Spinner';
import ChatHeader from '../ChatHeader';
import ChatMain from '../../Components/ChatMain';
import { IMessage, IUser } from '../../interfaces';
import { getMessages } from '../../Services/messageService';
import { getCurrentUser } from '../../Services/userService';
import ChatInput from '../ChatInput';
import { v4 as uuidv4 } from 'uuid';

import "./styles.css";

const Chat: React.FC = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [currentUser, setCurrentUser] = useState<IUser>();
  const [messages, setMessages] = useState<IMessage[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      const messages = await getMessages();
      setMessages(messages);
      const user = await getCurrentUser();
      setCurrentUser(user);

      setIsLoading(false);
    }
    fetchData();
  }, [])

  const addMessage = (messageBody: string) => {
    const message: IMessage = {
      id: uuidv4(),
      text: messageBody,
      user: currentUser!.name || "",
      userId: currentUser!.id,
      avatar: currentUser!.avatar,
      editedAt: "",
      createdAt: (new Date()).toISOString(),
      isLiked: false
    }
    setMessages([...messages, message]);
  }

  const toggleLike = (likedMessageId: string) => {
    const newMessages = messages.map(message => {
      if (message.id === likedMessageId)
        return {
          ...message,
          isLiked: !message.isLiked
        };
      return message;
    });
    setMessages(newMessages);
  }

  const deleteMessage = (deletedMessageId: string) => {
    const newMessages = messages.filter(message => message.id !== deletedMessageId);
    setMessages(newMessages);
  } 

  const editMessage = (editedMessageId: string, newText: string) => {
    const newMessages = messages.map(message => {
      if (message.id === editedMessageId)
        return {
          ...message,
          text: newText,
          editedAt: (new Date()).toISOString()
        };
      return message;
    });
    setMessages(newMessages);
  }

  return (
    <>
      {isLoading ?
        <Spinner />
        :
        <div className="chat-wrapper">
          <ChatHeader
            messages={messages}
            chatName="Chat name"
          />
          <ChatMain
            messages={messages}
            currentUser={currentUser!}
            messageFunction={{
              onLike: toggleLike,
              onDelete: deleteMessage, 
              onEdit: editMessage
            }}
          />
          <ChatInput
            sendMessage={addMessage}
          />
        </div>
      }
    </>
  );
}

export default Chat;
