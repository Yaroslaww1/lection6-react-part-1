import React from 'react';
import PropTypes from 'prop-types';
import { IMessage } from '../../interfaces';
import { ChatHeader as ChatHeaderComponent } from '../../Components/ChatHeader';

type ChatHeaderProps = {
  messages: IMessage[],
  chatName: string
}

const getLastMessageTime = (messages: IMessage[]) => {
  const lastMessage = messages.reduce(function(m1, m2) {
    const date1 = new Date(m1.createdAt);
    const date2 = new Date(m2.createdAt);
    return date1 > date2 ? m1 : m2;
  });
  return new Date(lastMessage.createdAt);
}

const getParticipantsCount = (messages: IMessage[]) => {
  const participantsMap: Record<string, boolean> = {};
  for (const message of messages) {
    participantsMap[message.userId] = true;
  }
  return Object.keys(participantsMap).length;
}

const ChatHeader: React.FC<ChatHeaderProps> = ({
  messages,
  chatName
}) => {
  return (
    <ChatHeaderComponent
      chatName={chatName}
      messagesCount={messages.length}
      participantsCount={getParticipantsCount(messages)}
      lastMessageTime={getLastMessageTime(messages)}
    />
  );
}

ChatHeader.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isLiked: PropTypes.bool.isRequired
  }).isRequired).isRequired,
  chatName: PropTypes.string.isRequired
}

export default ChatHeader;
