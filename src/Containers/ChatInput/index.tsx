import React, { useState } from 'react';
import PropTypes from 'prop-types';

import "./styles.css";

type ChatInputProps = {
  sendMessage: (message: string) => void
}

const ChatInput: React.FC<ChatInputProps> = ({
  sendMessage: propsSendMessage,
}) => {
  const [message, setMessage] = useState<string>('');

  const handleChange = (event: React.FormEvent<HTMLTextAreaElement>) => {
    const message = event.currentTarget.value;
    setMessage(message);
  }

  const sendMessage = () => {
    propsSendMessage(message);
    setMessage('');
  }

  return (
    <div className="chat-input-wrapper">
      <textarea
        placeholder="Enter your message here"
        value={message}
        onChange={handleChange}
      />
      <button
        onClick={sendMessage}
      >
        Send
        <i className="fa fa-paper-plane" aria-hidden="true"></i>
      </button>
    </div>
  );
}

ChatInput.propTypes = {
  sendMessage: PropTypes.func.isRequired
}

export default ChatInput;
