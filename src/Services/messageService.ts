import { IMessage } from "../interfaces";

const API_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

export async function getMessages(): Promise<IMessage[]> {
  const request = await fetch(API_URL, {
    method: 'GET'
  });
  const messages = await request.json() as IMessage[];
  return messages;
}