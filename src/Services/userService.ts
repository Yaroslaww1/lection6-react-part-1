import { IUser } from "../interfaces";
import { v4 as uuidv4 } from 'uuid';

export async function getCurrentUser(): Promise<IUser> {
  return {
    id: uuidv4(),
    name: "Yaroslav",
    avatar: ""
  };
}