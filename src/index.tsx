import React from 'react';
import ReactDOM from 'react-dom';
import Chat from './Containers/Chat';
import PageHeader from './Components/PageHeader';
import PageFooter from './Components/PageFooter';
import * as serviceWorker from './serviceWorker';

import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <>
      <PageHeader />
      <Chat />
      <PageFooter />
    </>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
