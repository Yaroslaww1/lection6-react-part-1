export interface IMessage {
  id: string,
  text: string,
  user: string,
  userId: string,
  avatar: string,
  editedAt: string,
  createdAt: string,
  isLiked: boolean
}

export interface IUser {
  id: string
  name: string
  avatar: string
}